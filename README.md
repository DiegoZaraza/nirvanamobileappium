# Test Mobile with Selenium and Appium


## Required Software

- Java JDK 11+
- Maven installed and in your classpath
- Appium
- Android Studio
- XCode

## How to execute the test

You can open eache test class on `src\main\java` and execute all of them, but I recommend you run it by the command line.

With command Line you can use 

For specific device, you can use

```
mvn test -Ddevice=IOS
``` 

options 

```
mvn test -Denv=IOS
mvn test -Denv=ANDROID
mvn test -Denv=IOS_BROWSERSTACK
mvn test -Denv=ANDROID_BROWSERSTACK
```

## Libraries

- Appium to test MOBILE DEVICES
- TestNG to support the test creation
- java-faker to generate fake data