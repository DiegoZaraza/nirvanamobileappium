package page;


import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePageMobile {
    protected final AppiumDriver driver;
    public WebDriverWait wait;

    public BasePageMobile(AppiumDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void click(By element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.findElement(element).click();
    }

    public void sendKeys(By element, String txt) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.findElement(element).sendKeys(txt);
    }

    public String getAttribute(By element, String attribute) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return driver.findElement(element).getAttribute(attribute);
    }

    public String getText(By element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return getAttribute(element, "text");
    }
}
