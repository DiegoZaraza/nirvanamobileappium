package AppiumSupport;

import com.browserstack.local.Local;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import utilities.PropertyUtils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class AppiumController {

    //Browserstack setup
    private static String BS_USERNAME = PropertyUtils.getProperty("BROWSERSTACK_USERNAMNE");
    private static String BS_ACCESSKEY = PropertyUtils.getProperty("BROWSERSTACK_ACCESS_KEY");
    private static String IOS_HASHED_APP_ID = "444bd0308813ae0dc236f8cd461c02d3afa7901d";
    private static String ANDROID_HASHED_APP_ID = "c700ce60cf13ae8ed97705a55b8e022f13c5827c";
    Local local = new Local();

    public static OS executionOS;

    public enum OS {
        ANDROID,
        IOS,
        ANDROID_BROWSERSTACK,
        IOS_BROWSERSTACK
    }
    public static AppiumController instance = new AppiumController();
    public WebDriver driver;

    public void start(){
        HashMap<String, String> options = optionsVPN();

        if (driver != null) {
            return;
        }
        DesiredCapabilities capabilities = new DesiredCapabilities();
        switch(executionOS){
            case ANDROID:
                capabilities.setCapability("platformName", "Android");
                capabilities.setCapability("deviceName", "device");
                try {
                    driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                break;
            case IOS:
                capabilities.setCapability("platformName", "ios");
                capabilities.setCapability("deviceName", "iPhone 7");
                capabilities.setCapability("automationName", "XCUITest");
                try {
                    driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                break;
            case ANDROID_BROWSERSTACK:
                System.out.println("Entro Start " + executionOS);

                capabilities.setCapability("browserName", "Android");
                capabilities.setCapability("device", "Samsung Galaxy S21 Ultra");
                capabilities.setCapability("realMobile", "true");
                capabilities.setCapability("os_version", "11.0");
                capabilities.setCapability("name", "Test BrowserStack Android"); // test name
                capabilities.setCapability("browserstack.local", "true");

                try {
                    local.start(options);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                try {
                    driver = new RemoteWebDriver(new URL("https://" + BS_USERNAME + ":" + BS_ACCESSKEY + "@hub-cloud.browserstack.com/wd/hub"), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                break;
            case IOS_BROWSERSTACK:
                capabilities.setCapability("browserName", "iPhone");
                capabilities.setCapability("os_version", "15");
                capabilities.setCapability("realMobile", "true");
                capabilities.setCapability("device", "iPhone 11 Pro");
                capabilities.setCapability("name", "Test BrowserStack iOS");
                capabilities.setCapability("browserstack.local", "true");

                try {
                    local.start(options);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                try {
                    driver = new RemoteWebDriver(new URL("https://" + BS_USERNAME + ":" + BS_ACCESSKEY + "@hub-cloud.browserstack.com/wd/hub"), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                break;
        }
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    private HashMap<String, String> optionsVPN(){
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("key", BS_ACCESSKEY);
        return options;
    }

    public void stop() throws Exception {
        local.stop();
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
