package AppiumSupport;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebDriver;

public abstract class AppiumBaseClass {
    protected WebDriver driver() { return AppiumController.instance.driver; }
}
