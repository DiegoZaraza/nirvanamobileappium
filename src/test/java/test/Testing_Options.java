package test;

import AppiumSupport.AppiumBaseClass;
import AppiumSupport.AppiumController;
import org.testng.Assert;
import org.testng.annotations.*;
import objects.web.HomePage;

public class Testing_Options extends AppiumBaseClass {
    HomePage homePage;

    @BeforeClass
    @Parameters({"device"})
    public void setupApplication(String device) {
        switch(device) {
            case "ANDROID":
                AppiumController.executionOS =AppiumController.OS.ANDROID;
                break;
            case "ANDROID_BROWSERSTACK":
                AppiumController.executionOS =AppiumController.OS.ANDROID_BROWSERSTACK;
                break;
            case "IOS":
                AppiumController.executionOS =AppiumController.OS.IOS;
                break;
            case "IOS_BROWSERSTACK":
                AppiumController.executionOS =AppiumController.OS.IOS_BROWSERSTACK;
                break;
        }

        AppiumController.instance.start();

        switch (AppiumController.executionOS) {
            case ANDROID:
            case ANDROID_BROWSERSTACK:
                homePage = new HomePage(driver());
                System.out.println("Android");
                break;
            case IOS:
            case IOS_BROWSERSTACK:
                homePage = new HomePage(driver());
                System.out.println("iOS");
                break;
        }

        driver().get("https://webapp.staging.internal.nirvana.tech/signin");
    }
    @Test
    public void test01() throws Exception {
        homePage.setUserName("Prueba@prueba.com");
        homePage.setPassword("Prueba1234");
        homePage.clickBtnSignIn();

        Assert.assertEquals(homePage.getLabelError(), "Incorrect username or password");
    }

    @Test
    public void test_SignUp(){
        homePage.clickBtnSignUp();
    }

    @AfterClass
    public void tearDown() {
        try {
            AppiumController.instance.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
