package objects.web;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.BasePageWeb;

public class HomePage extends BasePageWeb {
    WebDriver driver;
    @FindBy(id = "username")
    WebElement txtUserName;

    @FindBy(id = "password")
    WebElement txtPassword;

    @FindBy(xpath = "//div[contains(text(), 'Sign In')]")
    WebElement btnSignIn;

    @FindBy(xpath = "//div[contains(text(), 'Sign Up')]")
    WebElement btnSignUp;

    @FindBy(xpath = "//p[contains(text(), 'Incorrect username or password')]")

    WebElement labelError;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void setUserName(String value) {
        clearTxt(txtUserName, "Clear txt until introduce a new username");
        sendKeys(txtUserName, value, "Input text for find a product");
    }

    public void setPassword(String value) {
        clearTxt(txtPassword, "Clear txt until introduce a new password");
        sendKeys(txtPassword, value, "Input text for find a product");
    }

    public void clickBtnSignIn() {
        click(btnSignIn, "Click on button find");
    }

    public void clickBtnSignUp() {
        click(btnSignUp, "Click on button find");
    }

    public String getLabelError() {
        waitForVisibility(labelError);
        return getText(labelError, "Get text message"); }
}
